const SPREADSHEET_ID = "1-Z3eN1L0jHnDlIQaAiIqH77JMihLC7FBih8z6-g0vhY";
const SPREADSHEET_SHEET = "verb30";
const SPREADSHEET_RANGE = "A1:E118";

const authorizeButton = document.querySelector("#authorize");
const signoutButton = document.querySelector("#signout");
const loadingSpan = document.querySelector("#loading");
const app = document.querySelector("#app");

const shuffle = array => {
  const shadow = [...array];
  for (let i = shadow.length - 1; i >= 0; i--) {
    const rand = Math.floor(Math.random() * (i + 1));
    [shadow[i], shadow[rand]] = [shadow[rand], shadow[i]];
  }
  return shadow;
};

const showHideAuthorizeButtons = isSignedIn => {
  if (isSignedIn) {
    authorizeButton.style.display = "none";
    signoutButton.style.display = "";
    loadingSpan.style.display = "none";
  } else {
    authorizeButton.style.display = "";
    signoutButton.style.display = "none";
    loadingSpan.style.display = "none";
  }
};

const initClient = () => {
  return gapi.client
    .init({
      apiKey: "AIzaSyD35fVf4ZsH19LwFNaAZDpdyZeflr93ZhY",
      clientId:
        "223858788158-bbfl70r6iuhfpmn3huh5d9c48vrn7cp4.apps.googleusercontent.com",
      discoveryDocs: [
        "https://sheets.googleapis.com/$discovery/rest?version=v4"
      ],
      scope: "https://www.googleapis.com/auth/spreadsheets"
    })
    .catch(error => {
      console.log(JSON.stringify(error, null, 2));
    });
};

const addEventToAuthorizeButtons = () => {
  authorizeButton.addEventListener("click", () => {
    gapi.auth2.getAuthInstance().signIn();
  });

  signoutButton.addEventListener("click", () => {
    gapi.auth2.getAuthInstance().signOut();
  });
};

const toggleViewWithSignInState = () => {
  showHideAuthorizeButtons(gapi.auth2.getAuthInstance().isSignedIn.get());
  gapi.auth2.getAuthInstance().isSignedIn.listen(showHideAuthorizeButtons);
};

const formatFetchedData = fetchedData => {
  return fetchedData.map(item => {
    return {
      id: item[0],
      ja: item[1],
      en: item[2],
      keyword: item[3],
      count: item[4]
    };
  });
};

const fetchExamples = () => {
  return gapi.client.sheets.spreadsheets.values.get({
    spreadsheetId: SPREADSHEET_ID,
    range: `${SPREADSHEET_SHEET}!${SPREADSHEET_RANGE}`
  });
};

const render = string => {
  app.textContent = string;
};

gapi.load("client:auth2", () => {
  initClient()
    .then(addEventToAuthorizeButtons)
    .then(toggleViewWithSignInState)
    .then(() => {
      fetchExamples()
        .then(response => {
          if (response.result.values.length > 0) {
            app.innerHTML = shuffle(formatFetchedData(response.result.values))
              .slice(0, 10)
              .map((item, index) => {
                return `
                  <div class="index">${index + 1}</div>
                  <div class="card">
                    ${item.ja}
                  </div>
                  <div class="card">
                    ${item.en}
                  </div>
                  <div class="submit-area">
                    <span class="ng-count">NG count: <span class="ng-count-id-${
                      item.id
                    }">${item.count}</span></span>
                    <button class="button submit" data-id="${
                      item.id
                    }" data-count="${item.count}">
                      Increment NG count
                    </button>
                  </div>
                  <div style="height: 3rem;"></div>
                `;
              })
              .join("\n");

            Array.from(document.querySelectorAll(".submit")).forEach(
              submitButton => {
                const id = submitButton.dataset.id;
                const count = parseInt(submitButton.dataset.count, 10);
                const newCount = count + 1;

                submitButton.addEventListener(
                  "click",
                  () => {
                    console.log("CLICKED");
                    gapi.client.sheets.spreadsheets.values
                      .update(
                        {
                          spreadsheetId: SPREADSHEET_ID,
                          range: `${SPREADSHEET_SHEET}!E${id}`,
                          valueInputOption: "RAW"
                        },
                        {
                          values: [[newCount]]
                        }
                      )
                      .then(response => {
                        console.log(response);
                        document.querySelector(
                          `.ng-count-id-${id}`
                        ).textContent = `${newCount}`;
                      })
                      .catch(response => {
                        console.log(`Error: ${response.result.error.message}`);
                      });
                  },
                  {
                    once: true
                  }
                );
              }
            );
          } else {
            console.log("No data found.");
          }
        })
        .catch(response => {
          console.log(`Error: ${response.result.error.message}`);
        });
    });
});
